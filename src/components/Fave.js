import React, { Component, useState } from "react";

class Fave extends Component {
  // state = {
  //   isFave: false
  // };

  handleClick = e => {
    e.stopPropagation();
    console.log("handling Fave click!");
    // this.setState(prevState => ({
    //   isFave: !prevState.isFave
    // }));
    this.props.onFaveToggle()
  };

  render() {
    // console.log('this is Fave:isFave' , this.props)
    const className = this.props.isFave ? "remove_from_queue" : "add_to_queue";
    return (
      <div className={`film-row-fave ${className}`} onClick={this.handleClick}>
        <p className="material-icons">{className}</p>
      </div>
    );
  }
}


export default Fave